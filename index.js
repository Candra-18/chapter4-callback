// 1. buat array of object students ada 5 data cukup
let students = [
  {
    name: "Mahar",
    province: "Jawa Timur",
    age: 24,
    status: "nikah",
  },
  {
    name: "Dhika",
    province: "Jawa Tengah",
    age: 20,
    status: "single",
  },
  {
    name: "Chandra",
    province: "Jawa Barat",
    age: 19,
    status: "single",
  },
  {
    name: "Nur",
    province: "NTT",
    age: 23,
    status: "nikah",
  },
  {
    name: "Ikhsan",
    province: "Ambon",
    age: 19,
    status: "single",
  },
];

// 2. 3 function, 1 function nentuin provinsi kalian ada di jawa barat gak, umur kalian diatas 22 atau gak, status kalian single atau gak
function provinceJawaBarat(province) {
  return province == "Jawa Barat" ? province : "jika bukan tinggal di Jawa Barat";
}

function AgeGreater22(age) {
  return age < 22 ? "masih berumur di bawah 22" : "lebih dari sama dengan 22";
}

function Single(status) {
  return status == "single" ? "single" : "merry";
}

// 3. callback function untuk print hasil proses 3 function diatas.
function locationStudents(callback, callback1, callback2) {
  for (const element of students) {
    // console.log(element);
    let province = callback(element.province);
    // console.log(element.provinsi);
    let age = callback1(element.age);
    let status = callback2(element.status);
    if (province == "Jawa Barat" && age == "masih berumur di bawah 22" && status == "single") {
      console.log(`Nama Saya adalah : ${element.name}, Saya berada di ${province}, Umur Saya ${age}, dan status saya ${status}`);
    }
  }
}

locationStudents(provinceJawaBarat, AgeGreater22, Single);

// nama saya imam, saya tinggal di jawa baRAT, umur saya dibawah 22 tahun. dan status saya single loh. CODE buat Nilam
